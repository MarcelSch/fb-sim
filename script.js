let play = false,
	hour = 0,
	SPEED = 100;
	;

const 	OWNBIKE_PRICE = 500,
	  	FAIRBIKE_PRICE = 200,
	  	AVG_REPAIR = 25,
	  	DAILY_CHANCE_RIDE = 2/3,
	  	DAILY_CHANCE_BREAK = 1/100,
	  	DAILY_CHANCE_DIE = 1/100,
	  	CHANCE_CITIZEN_HAS_BIKE = 9/10,
	  	displayTime = () => {
			$("#year").html(Math.round(hour/(24*365)));
			$("#day").html(Math.round(hour/24));
			$("#hour").html(hour);
		},
		rand = (_min, _max) => {
			const min = _min || 0;
			const max = _max || 1;

			this.float =
			this.value = Math.random() * (max - min) + min;
			this.int = Math.round(this.value);

			return this;
		},
		draw = (chance) => {
			return (rand(0,1).value <= chance);
		},
		randPerson = () => {
			const arr = ["👶","🧒","👦","🧑","👱‍♀️","🧔","🧓","👲","👳‍♂️","👮‍♀️","👷‍♀️","💂‍♀️","🕵️‍♀️","👩‍⚕️","👩‍🌾","👨‍🌾","👨‍🍳","👨‍🎓","👨‍🎤","👩‍🏭","👩‍💻","👩‍💼","👩‍🔧","👩‍🔬","👩‍🎨","👩‍🚒","👩‍✈️","👩‍🚀","👩‍⚖️","👰","👸","🤶","🧙‍♀️","🧝‍♀️","🧛‍♀️","🧟‍♀️","🧟‍♂️","🧖‍♂️"];
			return arr[rand(0,arr.length-1).int];
		},
		randBuilding = () => {
			const arr = ["🚋","🚞","🚝","🚄","🚅","🚈","🚆","🚇","🚊","🚉","🛫","⛽️","🚏","🗽","🗼","🏰","🏯","🏟","🎡","🎢","🎠","⛲️","⛱","🏖","🏕","⛺️","🏠","🏡","🏘","🏚","🏗","🏭","🏬","🏢","🏣","🏤","🏥","🏦","🏨","🏪","🏩","🏫","💒","🏛","⛪️","🕌","🕍","🕋","⛩","🛤","🛣"];
			return arr[rand(0,arr.length-1).int];
		},
		init = () => {
			
			//make instance of 1 location
			//make instance of 1 shop
			//make instance of 100 people
			//make instance of 100 ownBikes

			let home = (new Location()).init();
			let shop = (new Shop()).init(home);
			let citizens = [];
			for (var i = 0; i < 10; i++) {
				let c = new Citizen(home);
				c.init();
				citizens.push(c);
			}
			
		},
		loop = () => {
			if(!play) return;
			hour++;
			
			//emit events of time passed
			if(hour % 24 == 0) 		 $(document).trigger("dayPassed");
			if(hour % (24*365) == 0) $(document).trigger("yearPassed");
			$(document).trigger("hourPassed");

			//display time
			displayTime();

			//stage next loop
			setTimeout(loop, SPEED);
		}
		;


function Location(_parentEl) {
	const self = this;

	const parentEl = _parentEl || "body";


	this.el;
	this.allOwnBikes = 0;
	this.allSharedBikes = 0;
	this.allFairBikes = 0;
	
	let onDay = () => { console.log("location DAY passed")};
	let onYear = () => { console.log("location YEAR passed")};

	function createEl(){
		self.el = $("<div>location</div>")
				.addClass("location")
				.appendTo(parentEl)
				;
	}
				
	this.init = () => {
		$(document).on("dayPassed",onDay);
		$(document).on("yearPassed",onYear);
		createEl();
		return this;
	}
}

function Citizen(_location){
	const self = this;

	let ownBike = null;
	const location = _location;

	this.el;

	let onDay = () => { 
		if(draw(DAILY_CHANCE_RIDE)){
			if(ownBike){
				//ride your own bike
			} else {
				//find shared or fair bike
			}
		}
	};
	
	let onYear = () => { console.log("Citizen YEAR passed")};

	function createEl(){
		self.el = $("<div></div")
				.addClass("citizen")
				.html(randPerson())
				.appendTo(location.el);
	}
				
	this.init = () => {
		$(document).on("dayPassed",onDay);
		$(document).on("yearPassed",onYear);
		createEl();

		if(draw(CHANCE_CITIZEN_HAS_BIKE)){
			ownBike = new OwnBike(this);
			ownBike.init();
		}
		return this;
	}
}

function Shop(_location){
	this.location = _location;
	this.earnedFromCitizens = 0;
	this.earnedFromFairbikes = 0;
	this.init = () => {

	}
}

function OwnBike(_owner){
	const self = this;

	this.el;

	const owner = _owner;

	let onDay = () => { console.log("ownBike DAY passed")};
	let onYear = () => { console.log("ownBike YEAR passed")};

	function createEl(){
		self.el = $("<div></div>")
					.addClass("bike")
					.html("🚲")
					.appendTo(owner.el);
	}
				
	this.init = () => {
		$(document).on("dayPassed",onDay);
		$(document).on("yearPassed",onYear);
		createEl();
		return this;
	}

}
function FairBike(){

	let onYear = () => { console.log("FairBike YEAR passed")};
	let onDay = () => { console.log("FairBike DAY passed")};	
	let onHour = () => { };
	
	this.init = () => {
		$(document).on("dayPassed",onDay);
		$(document).on("hourPassed",onHour);
		$(document).on("yearPassed",onYear);
		return this;
	}

}
function ShareBike(){

	let onYear = () => { console.log("ShareBike YEAR passed")};
	let onDay = () => { console.log("ShareBike DAY passed")};
	let onHour = () => { };
	
	this.init = () => {
		$(document).on("dayPassed",onDay);
		$(document).on("hourPassed",onHour);
		$(document).on("yearPassed",onYear);
		return this;
	}

}



$(document).ready(function() {

	init();

	$("#play-pause")
		.html("start")
		.on("click",(evt) => {
			play = (!play);
			if(play) loop();
			$(evt.target).html(play ? "pause" : "play");
		})
	
	$("#reset").on("click",() => location.reload());

});


