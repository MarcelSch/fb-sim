jQuery(document).ready(function($) {

	const HOME = "#home";
	
	const history_back = (evt) => {

		evt.preventDefault();

		if(evt.target.length > 1){
			navigateToScreen();
			return;
		}
		
		window.history.go(-1);
		
	};

	const navigateToScreen = () => {

		const hash = window.location.hash;
		const dest = (hash.length <= 1) ? HOME : hash;
		
		$(".screen").fadeOut(400);
		$(dest).fadeIn(400);

	}

	$(window).on("hashchange", navigateToScreen);
	$(".menu-back").on("click", history_back);
	
	$(".screen").hide();
	navigateToScreen();

});